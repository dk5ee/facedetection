# load the library using the import keyword
# OpenCV must be properly installed for this to work. If not, then the module will not load with an
# error message.

import cv2
import sys
import glob
import os
import math
import numpy as np

# improves image, source:  https://gist.github.com/DavidYKay/9dad6c4ab0d8d7dbf3dc

def apply_mask(matrix, mask, fill_value):
    masked = np.ma.array(matrix, mask=mask, fill_value=fill_value)
    return masked.filled()

def apply_threshold(matrix, low_value, high_value):
    low_mask = matrix < low_value
    matrix = apply_mask(matrix, low_mask, low_value)

    high_mask = matrix > high_value
    matrix = apply_mask(matrix, high_mask, high_value)

    return matrix

def simplest_cb(img, percent):
    assert img.shape[2] == 3
    assert percent > 0 and percent < 100

    half_percent = percent / 200.0

    channels = cv2.split(img)

    out_channels = []
    for channel in channels:
        assert len(channel.shape) == 2
        # find the low and high precentile values (based on the input percentile)
        height, width = channel.shape
        vec_size = width * height
        flat = channel.reshape(vec_size)

        assert len(flat.shape) == 1

        flat = np.sort(flat)

        n_cols = flat.shape[0]

        low_val  = flat[int(math.floor(n_cols * half_percent))]
        high_val = flat[int(math.ceil( n_cols * (1.0 - half_percent)))]

        print "Lowval: ", low_val
        print "Highval: ", high_val

        # saturate below the low percentile and above the high percentile
        thresholded = apply_threshold(channel, low_val, high_val)
        # scale the channel
        normalized = cv2.normalize(thresholded, thresholded.copy(), 0, 255, cv2.NORM_MINMAX)
        out_channels.append(normalized)

    return cv2.merge(out_channels)

# Gets the name of the image file (filename) from sys.argv

def overlay_image_alpha(img, img_overlay, pos, alpha_mask):
    """Overlay img_overlay on top of img at the position specified by
    pos and blend using alpha_mask.

    Alpha mask must contain values within the range [0, 1] and be the
    same size as img_overlay.
    """

    x, y = pos

    # Image ranges
    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

    # Overlay ranges
    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

    # Exit if nothing to do
    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return

    channels = img.shape[2]

    alpha = alpha_mask[y1o:y2o, x1o:x2o]
    alpha_inv = 1.0 - alpha

    for c in range(channels):
        img[y1:y2, x1:x2, c] = (alpha * img_overlay[y1o:y2o, x1o:x2o, c] +
                                alpha_inv * img[y1:y2, x1:x2, c])

imagePath = sys.argv[1]
THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
imgovers = cv2.imread("gradient_s.png", -1)
imgoverm = cv2.imread("gradient_m.png", -1)
imgoverl = cv2.imread("gradient_l.png", -1)

def getimage(A):
	if A> 400:
		return imgoverl
	else:
		if A>175:
			return imgoverm
		else:
			return imgovers

image = cv2.imread(imagePath)
# scale image

myscale = 1080.0 / image.shape[0] # scale to max height
width = int(image.shape[1] * myscale)
height = int(image.shape[0] * myscale)
dim = (width, height) 
#image = simplest_cb(image, 1)
resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA) 
# The image is read and converted to grayscale
resized2 =resized[1:1008, 1:1440]
image2 = simplest_cb(resized2, 1)
gray = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
newImage = resized.copy()

for cascPath in glob.glob(THIS_FOLDER+ '/*.xml'):
	#cascPath = "haarcascade_frontalface_default.xml"
	print ("detecting "+cascPath)
	# This creates the cascade classifcation from file 

	faceCascade = cv2.CascadeClassifier(cascPath)
	

	
	# The face or faces in an image are detected
	# This section requires the most adjustments to get accuracy on face being detected.
	
	
	faces = faceCascade.detectMultiScale(
	gray,
	scaleFactor=1.05,
	minNeighbors=4,
	minSize=(10,10),
	flags = cv2.CASCADE_SCALE_IMAGE
	)
	
	print("Detected {0} faces!".format(len(faces)))

	for (x, y, w, h) in faces:
		img_small=cv2.resize(getimage(h), (3*w,3*h))
		overlay_image_alpha(newImage, img_small[:, :, 0:3], (x-w, y-h), img_small[:, :, 3] / 255.0)
		
print ("processed/"+imagePath);
cv2.imwrite("processed/"+imagePath,newImage);
#cv2.imshow("Faces Detected", image)
#cv2.waitKey(0)
